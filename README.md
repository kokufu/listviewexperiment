ListViewExperiment
===============
ListView を使用している際に、Adapter#getView() が呼ばれるタイミングを調べるプロジェクト。
ブログ記事は以下。

[http://kokufu.blogspot.jp/2012/02/getview.html](http://kokufu.blogspot.jp/2012/02/getview.html)

[http://kokufu.blogspot.jp/2012/03/listview-graphical-layout.html](http://kokufu.blogspot.jp/2012/03/listview-graphical-layout.html)

## Eclispse へのインポート方法
New -> Project -> Android Project from Existing Code

