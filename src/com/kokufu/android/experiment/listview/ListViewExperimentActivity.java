
package com.kokufu.android.experiment.listview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.kokufu.android.experiment.listview.R;

public class ListViewExperimentActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new MyAdapter(getApplicationContext()));
    }
}
