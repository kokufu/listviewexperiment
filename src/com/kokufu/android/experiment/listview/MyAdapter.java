
package com.kokufu.android.experiment.listview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {
    private static final String TAG = "MyAdapter";

    private final LayoutInflater inflater;
    private int inflatedViewNo = 0;

    public MyAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return 100;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(android.R.layout.two_line_list_item, parent, false);
            // 通し番号をTagにセットする
            view.setTag(inflatedViewNo);
            Log.d(TAG,
                    "Inflate " + inflatedViewNo + "  (position: " + position + ")  "
                            + view.hashCode());
            inflatedViewNo++;
        } else {
            view = convertView;
        }

        {
            TextView tv1 = (TextView) view.findViewById(android.R.id.text1);
            StringBuilder sb = new StringBuilder("position : ");
            sb.append(getItem(position));
            tv1.setText(sb);
        }

        {
            TextView tv2 = (TextView) view.findViewById(android.R.id.text2);
            StringBuilder sb = new StringBuilder("Inflate : ");
            sb.append((Integer) view.getTag());
            tv2.setText(sb);
        }

        Log.d(TAG, "Display (position: " + position + ")  " + view.hashCode());

        return view;
    }

}
